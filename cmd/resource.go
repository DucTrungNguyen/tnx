package cmd

import (
	"errors"
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/DucTrungNguyen/tnx/helpers"
	"gitlab.com/DucTrungNguyen/tnx/tpl"
)

// resourceCmd represents the resource command
var resourceCmd = &cobra.Command{
	Use:   "resource name",
	Short: "Create a new REST resource",
	Long:  `Create a new REST resource`,
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			return errors.New("consumer name is required")
		}
		createResource(args[0])
		return nil
	},
}

var resourceModuleFlag string

type Resource struct {
	Name string
}

func init() {
	rootCmd.AddCommand(resourceCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// resourceCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	resourceCmd.Flags().StringVarP(&resourceModuleFlag, "module", "m", "", "Module name, eg: public")
}

func createResource(name string) {
	fileName := helpers.ToSnakeCase(name)
	file := fmt.Sprintf("resources/%s.go", fileName)
	resource := &Resource{
		Name: name,
	}
	if len(resourceModuleFlag) > 0 {
		file = fmt.Sprintf("src/%s/%s", resourceModuleFlag, file)
	}
	helpers.GenerateFile(file, tpl.Resource(), resource)
	fmt.Println("Create resource", name, "with file name: ", file, "successfully.")
	fmt.Println("Resource", file, "was created.")
}
