package cmd

import (
	"errors"
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/DucTrungNguyen/tnx/helpers"
	"gitlab.com/DucTrungNguyen/tnx/tpl"
)

// consumerCmd represents the consumer command
var consumerCmd = &cobra.Command{
	Use:   "consumer name",
	Short: "Create a new kafka consumer",
	Long:  `Create a new kafka consumer`,
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			return errors.New("consumer name is required")
		}
		createConsumer(args[0])
		return nil
	},
}

var consumerModuleFlag string

type Consumer struct {
	Name string
}

func init() {
	rootCmd.AddCommand(consumerCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// consumerCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	consumerCmd.Flags().StringVarP(&consumerModuleFlag, "module", "m", "", "Name of module, eg: worker")
}

func createConsumer(name string) {
	fileName := helpers.ToSnakeCase(name)
	file := fmt.Sprintf("consumers/%s.go", fileName)
	consumer := &Consumer{
		Name: name,
	}
	if len(consumerModuleFlag) > 0 {
		file = fmt.Sprintf("src/%s/%s", consumerModuleFlag, file)
	}
	helpers.GenerateFile(file, tpl.Consumer(), consumer)
	fmt.Println("Consumer", file, "was created.")
}
