package cmd

import (
	"errors"
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/DucTrungNguyen/tnx/helpers"
	"gitlab.com/DucTrungNguyen/tnx/tpl"
)

// initCmd represents the init command
var initCmd = &cobra.Command{
	Use:   "init <module>",
	Short: "Init Go project with golibs-starter libraries",
	Long:  `Init Go project with golibs-starter libraries`,
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			return errors.New("init needs a module name for the command")
		}
		initProjectFiles(args[0])
		return nil
	},
}

var initModulesFlag []string

type Project struct {
	Module             string
	ServiceName        string
	HasCoreModule      bool
	HasAdapterModule   bool
	HasInternalModule  bool
	HasMigrationModule bool
	HasPublicModule    bool
	HasWorkerModule    bool
}

type Environment struct {
	Environment string
}

func init() {
	rootCmd.AddCommand(initCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// initCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	initCmd.Flags().StringSliceVarP(&initModulesFlag, "module", "m", []string{}, "Module name")
}

func initProjectFiles(module string) {
	if len(initModulesFlag) == 0 {
		initModulesFlag = []string{"adapter", "core", "internal", "migration", "public", "worker"}
	}

	project := &Project{
		Module:             module,
		ServiceName:        helpers.FromModuleToServiceName(module),
		HasCoreModule:      helpers.Contains(initModulesFlag, "core"),
		HasAdapterModule:   helpers.Contains(initModulesFlag, "adapter"),
		HasInternalModule:  helpers.Contains(initModulesFlag, "internal"),
		HasMigrationModule: helpers.Contains(initModulesFlag, "migration"),
		HasPublicModule:    helpers.Contains(initModulesFlag, "public"),
		HasWorkerModule:    helpers.Contains(initModulesFlag, "worker"),
	}

	// Common
	helpers.GenerateFile(".gitignore", tpl.Gitignore(), nil)
	helpers.GenerateFile(".gitlab-ci.yml", tpl.GitlabCI(), project)
	helpers.GenerateFile("Dockerfile", tpl.Dockerfile(), project)
	helpers.GenerateFile("Makefile", tpl.Makefile(), project)
	helpers.GenerateFile("docker-compose.yml", tpl.DockerCompose(), nil)
	helpers.GenerateFile("k8s/docker-build.yml", tpl.K8SDockerBuild(), nil)

	// Adapter
	if project.HasAdapterModule {
		adapterModule := &Project{Module: fmt.Sprintf("%s/%s", module, "adapter")}
		helpers.GenerateFile("src/adapter/go.mod", tpl.GoMod(), adapterModule)
	}

	// Core
	if project.HasCoreModule {
		coreModule := &Project{Module: fmt.Sprintf("%s/%s", module, "core")}
		helpers.GenerateFile("src/core/go.mod", tpl.GoMod(), coreModule)
	}

	// Internal
	if project.HasInternalModule {
		internalModule := &Project{Module: fmt.Sprintf("%s/%s", module, "internal")}
		helpers.GenerateFile("k8s/internal/config.yml", tpl.K8SWebserviceConfigmap(), nil)
		helpers.GenerateFile("k8s/internal/deploy.sh", tpl.K8SWebserviceDeploy(), nil)
		helpers.GenerateFile("k8s/internal/main.yml", tpl.K8SWebserviceMain(), nil)
		helpers.GenerateFile("src/internal/go.mod", tpl.GoMod(), internalModule)
		helpers.GenerateFile("src/internal/main.go", tpl.WebserviceMain(), internalModule)
		helpers.GenerateFile("src/internal/bootstrap/all.go", tpl.WebserviceBootstrapAll(), internalModule)
		helpers.GenerateFile("src/internal/bootstrap/build.go", tpl.BootstrapBuild(), nil)
		helpers.GenerateFile("src/internal/config/default.yml", tpl.ConfigDefault(), nil)
		helpers.GenerateFile("src/internal/config/local.yml", tpl.Config(), nil)
		helpers.GenerateFile("src/internal/config/prod.yml", tpl.Config(), nil)
		helpers.GenerateFile("src/internal/config/qc.yml", tpl.Config(), nil)
		helpers.GenerateFile("src/internal/config/uat.yml", tpl.Config(), nil)
		helpers.GenerateFile("src/internal/config/testing.yml", tpl.Config(), nil)
		helpers.GenerateFile("src/internal/routers/router.go", tpl.Router(), internalModule)
		helpers.GenerateFile("src/internal/properties/swagger_properties.go", tpl.SwaggerProperties(), nil)
		helpers.GenerateFile("src/internal/tests/actuator_test.go", tpl.ActuatorTest(), nil)
		helpers.GenerateFile("src/internal/tests/test_suite.go", tpl.WebserviceTestsuite(), internalModule)
	}

	// Migration
	if project.HasMigrationModule {
		migrationModule := &Project{Module: fmt.Sprintf("%s/%s", module, "migration")}
		helpers.GenerateFile("Dockerfile.migration", tpl.DockerfileMigration(), nil)
		helpers.GenerateFile("k8s/migration/config.yml", tpl.K8SMigrationConfigmap(), nil)
		helpers.GenerateFile("k8s/migration/migrate.sh", tpl.K8SMigrationScript(), nil)
		helpers.GenerateFile("k8s/migration/migrate.yml", tpl.K8SMigrationJob(), nil)
		helpers.GenerateFile("src/migration/go.mod", tpl.GoMod(), migrationModule)
		helpers.GenerateFile("src/migration/main.go", tpl.MigrationMain(), nil)
		helpers.GenerateFile("src/migration/config/default.yml", tpl.ConfigDefault(), nil)
		helpers.GenerateFile("src/migration/config/local.yml", tpl.Config(), nil)
		helpers.GenerateFile("src/migration/config/prod.yml", tpl.Config(), nil)
		helpers.GenerateFile("src/migration/config/qc.yml", tpl.Config(), nil)
		helpers.GenerateFile("src/migration/config/uat.yml", tpl.Config(), nil)
	}

	// Public
	if project.HasPublicModule {
		publicModule := &Project{Module: fmt.Sprintf("%s/%s", module, "public")}
		helpers.GenerateFile("k8s/public/config.yml", tpl.K8SWebserviceConfigmap(), nil)
		helpers.GenerateFile("k8s/public/deploy.sh", tpl.K8SWebserviceDeploy(), nil)
		helpers.GenerateFile("k8s/public/main.yml", tpl.K8SWebserviceMain(), nil)
		helpers.GenerateFile("src/public/go.mod", tpl.GoMod(), publicModule)
		helpers.GenerateFile("src/public/main.go", tpl.WebserviceMain(), publicModule)
		helpers.GenerateFile("src/public/bootstrap/all.go", tpl.WebserviceBootstrapAll(), publicModule)
		helpers.GenerateFile("src/public/bootstrap/build.go", tpl.BootstrapBuild(), nil)
		helpers.GenerateFile("src/public/config/default.yml", tpl.ConfigDefault(), nil)
		helpers.GenerateFile("src/public/config/local.yml", tpl.Config(), nil)
		helpers.GenerateFile("src/public/config/prod.yml", tpl.Config(), nil)
		helpers.GenerateFile("src/public/config/qc.yml", tpl.Config(), nil)
		helpers.GenerateFile("src/public/config/uat.yml", tpl.Config(), nil)
		helpers.GenerateFile("src/public/config/testing.yml", tpl.Config(), nil)
		helpers.GenerateFile("src/public/routers/router.go", tpl.Router(), publicModule)
		helpers.GenerateFile("src/public/properties/swagger_properties.go", tpl.SwaggerProperties(), nil)
		helpers.GenerateFile("src/public/tests/actuator_test.go", tpl.ActuatorTest(), nil)
		helpers.GenerateFile("src/public/tests/test_suite.go", tpl.WebserviceTestsuite(), publicModule)
	}

	// Worker
	if project.HasWorkerModule {
		workerModule := &Project{Module: fmt.Sprintf("%s/%s", module, "worker")}
		helpers.GenerateFile("k8s/worker/config.yml", tpl.K8SWebserviceConfigmap(), nil)
		helpers.GenerateFile("k8s/worker/deploy.sh", tpl.K8SWebserviceDeploy(), nil)
		helpers.GenerateFile("k8s/worker/main.yml", tpl.K8SWebserviceMain(), nil)
		helpers.GenerateFile("src/worker/go.mod", tpl.GoMod(), workerModule)
		helpers.GenerateFile("src/worker/main.go", tpl.WebserviceMain(), workerModule)
		helpers.GenerateFile("src/worker/bootstrap/all.go", tpl.WebserviceBootstrapAll(), workerModule)
		helpers.GenerateFile("src/worker/bootstrap/build.go", tpl.BootstrapBuild(), nil)
		helpers.GenerateFile("src/worker/config/default.yml", tpl.ConfigDefault(), nil)
		helpers.GenerateFile("src/worker/config/local.yml", tpl.Config(), nil)
		helpers.GenerateFile("src/worker/config/prod.yml", tpl.Config(), nil)
		helpers.GenerateFile("src/worker/config/qc.yml", tpl.Config(), nil)
		helpers.GenerateFile("src/worker/config/uat.yml", tpl.Config(), nil)
		helpers.GenerateFile("src/worker/config/testing.yml", tpl.Config(), nil)
		helpers.GenerateFile("src/worker/routers/router.go", tpl.Router(), workerModule)
		helpers.GenerateFile("src/worker/properties/swagger_properties.go", tpl.SwaggerProperties(), nil)
		helpers.GenerateFile("src/worker/tests/actuator_test.go", tpl.ActuatorTest(), nil)
		helpers.GenerateFile("src/worker/tests/test_suite.go", tpl.WebserviceTestsuite(), workerModule)
	}
}
