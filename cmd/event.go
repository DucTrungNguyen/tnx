package cmd

import (
	"errors"
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/DucTrungNguyen/tnx/helpers"
	"gitlab.com/DucTrungNguyen/tnx/tpl"
)

// eventCmd represents the event command
var eventCmd = &cobra.Command{
	Use:   "event name",
	Short: "Create a new Event",
	Long:  `Create a new Event`,
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			return errors.New("event name is required")
		}
		createEvent(args[0])
		return nil
	},
}

var eventModuleFlag string

type Event struct {
	Name string
}

func init() {
	rootCmd.AddCommand(eventCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// eventCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	eventCmd.Flags().StringVarP(&eventModuleFlag, "module", "m", "", "Event name, eg: ExampleEvent")
}

func createEvent(name string) {
	fileName := helpers.ToSnakeCase(name)
	file := fmt.Sprintf("events/%s.go", fileName)
	event := &Event{
		Name: name,
	}
	if len(eventModuleFlag) == 0 {
		eventModuleFlag = "core"
	}
	file = fmt.Sprintf("src/%s/%s", eventModuleFlag, file)
	helpers.GenerateFile(file, tpl.Event(), event)
	fmt.Println("Event", file, "was created.")
}
