package cmd

import (
	"errors"
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/DucTrungNguyen/tnx/helpers"
	"gitlab.com/DucTrungNguyen/tnx/tpl"
	"io/ioutil"
	"os"
	"regexp"
	"strconv"
)

// migrationCmd represents the migration command
var migrationCmd = &cobra.Command{
	Use:   "migration name",
	Short: "Create a new migration file",
	Long:  `Create a new migration file with auto detect current version.`,
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			return errors.New("controller name is required")
		}
		createMigration(args[0])
		return nil
	},
}

type MigrationAction string

const (
	CreateTableAction MigrationAction = "CreateTable"
)

type Migration struct {
	Name   string
	Action MigrationAction
	Table  string
}

func (m *Migration) getMigrationAction() {
	if ok, err := regexp.MatchString(`^create_.+_table$`, m.Name); ok && err == nil {
		m.Action = CreateTableAction
		return
	}
	return
}

func (m *Migration) getTableName() {
	switch m.Action {
	case CreateTableAction:
		m.Table = m.Name[7 : len(m.Name)-6]
		return
	default:
		return
	}
}

func init() {
	rootCmd.AddCommand(migrationCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// migrationCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// migrationCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

func createMigration(name string) {
	migrationPath := fmt.Sprintf("%s/src/migration/migrations", helpers.GetProjectAbsolutePath())
	if err := os.MkdirAll(migrationPath, 0754); err != nil {
		cobra.CheckErr(err)
	}
	files, err := ioutil.ReadDir(migrationPath)
	version := 0
	if err != nil && os.IsNotExist(err) {
		cobra.CheckErr(err)
	}
	for _, f := range files {
		v, err := strconv.Atoi(f.Name()[:4])
		if err != nil {
			cobra.CheckErr(err)
		}
		if v > version {
			version = v
		}
	}
	migration := &Migration{
		Name: name,
	}
	migration.getMigrationAction()
	migration.getTableName()
	migrationFile := fmt.Sprintf("%04d_%s.up.sql", version+1, name)
	helpers.GenerateFile(fmt.Sprintf("src/migration/migrations/%s", migrationFile), tpl.Migration(), migration)
	fmt.Println("Migration", migrationFile, "was created.")
}
