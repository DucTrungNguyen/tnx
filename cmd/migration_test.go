package cmd

import "testing"

func TestMigrationAction(t *testing.T) {
	m := &Migration{Name: "create_users_table"}
	m.getMigrationAction()
	if CreateTableAction != m.Action {
		t.Fail()
	}
}

func TestMigrationTable(t *testing.T) {
	m := &Migration{Name: "create_users_table"}
	m.getMigrationAction()
	m.getTableName()
	if m.Table != "users" {
		t.Fatalf("expect: %s, but got: %s", "users", m.Table)
	}
	m = &Migration{Name: "create_amazing_jobs_table"}
	m.getMigrationAction()
	m.getTableName()
	if m.Table != "amazing_jobs" {
		t.Fail()
	}
}
