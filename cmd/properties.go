package cmd

import (
	"errors"
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/DucTrungNguyen/tnx/helpers"
	"gitlab.com/DucTrungNguyen/tnx/tpl"
)

// propertiesCmd represents the properties command
var propertiesCmd = &cobra.Command{
	Use:   "properties",
	Short: "Create a new properties",
	Long:  `Create a new properties`,
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			return errors.New("properties name is required")
		}
		createProperties(args[0])
		return nil
	},
}

var (
	propertiesModuleFlag string
	propertiesPrefixFlag string
)

type Properties struct {
	Name   string
	Prefix string
}

func init() {
	rootCmd.AddCommand(propertiesCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// propertiesCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	propertiesCmd.Flags().StringVarP(&propertiesModuleFlag, "module", "m", "", "Name of module, eg: worker")
	propertiesCmd.Flags().StringVarP(&propertiesPrefixFlag, "prefix", "p", "", "Prefix of yaml file config, eg: app.worker")
}

func createProperties(name string) {
	fileName := helpers.ToSnakeCase(name)
	file := fmt.Sprintf("properties/%s.go", fileName)
	properties := &Properties{
		Name:   name,
		Prefix: propertiesPrefixFlag,
	}
	if len(propertiesModuleFlag) > 0 {
		file = fmt.Sprintf("src/%s/%s", propertiesModuleFlag, file)
	}
	helpers.GenerateFile(file, tpl.Properties(), properties)
	fmt.Println("Properties", file, "was created.")
}
