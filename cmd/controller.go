package cmd

import (
	"errors"
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/DucTrungNguyen/tnx/helpers"
	"gitlab.com/DucTrungNguyen/tnx/tpl"
)

// controllerCmd represents the controller command
var controllerCmd = &cobra.Command{
	Use:   "controller name",
	Short: "Create a new controller",
	Long:  `Create a new controller`,
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			return errors.New("controller name is required")
		}
		createController(args[0])
		return nil
	},
}

var controllerModuleFlag string

type Controller struct {
	Name string
}

func init() {
	rootCmd.AddCommand(controllerCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// controllerCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	controllerCmd.Flags().StringVarP(&controllerModuleFlag, "module", "m", "", "Name of module, eg: worker")
}

func createController(name string) {
	fileName := helpers.ToSnakeCase(name)
	file := fmt.Sprintf("controllers/%s.go", fileName)
	controller := &Controller{
		Name: name,
	}
	if len(controllerModuleFlag) > 0 {
		file = fmt.Sprintf("src/%s/%s", controllerModuleFlag, file)
	}
	helpers.GenerateFile(file, tpl.Controller(), controller)
	fmt.Println("Controller", file, "was created.")
}
