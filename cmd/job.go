package cmd

import (
	"errors"
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/DucTrungNguyen/tnx/helpers"
	"gitlab.com/DucTrungNguyen/tnx/tpl"
)

type Job struct {
	Name string
}

// jobCmd represents the job command
var jobCmd = &cobra.Command{
	Use:   "job",
	Short: "Create a new cron job",
	Long:  `Create a new cron job`,
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			return errors.New("consumer name is required")
		}
		createCronJob(args[0])
		return nil
	},
}

var jobModuleFlag string

func init() {
	rootCmd.AddCommand(jobCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// jobCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// jobCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

func createCronJob(name string) {
	fileName := helpers.ToSnakeCase(name)
	file := fmt.Sprintf("jobs/%s.go", fileName)
	model := &Model{
		Name: name,
	}
	if len(jobModuleFlag) == 0 {
		jobModuleFlag = "worker"
	}
	if len(jobModuleFlag) > 0 {
		file = fmt.Sprintf("src/%s/%s", jobModuleFlag, file)
	}
	helpers.GenerateFile(file, tpl.Job(), model)
	fmt.Println("Job", file, "was created.")
}
