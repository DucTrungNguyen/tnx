package cmd

import (
	"errors"
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/DucTrungNguyen/tnx/helpers"
	"gitlab.com/DucTrungNguyen/tnx/tpl"
)

// repositoryCmd represents the repository command
var repositoryCmd = &cobra.Command{
	Use:   "repository",
	Short: "Create a new Gorm repository",
	Long:  `Create a new Gorm repository`,
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			return errors.New("consumer name is required")
		}
		createRepository(args[0])
		return nil
	},
}

var repositoryModuleFlag string

type Repository struct {
	Name string
}

func init() {
	rootCmd.AddCommand(repositoryCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// repositoryCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	repositoryCmd.Flags().StringVarP(&repositoryModuleFlag, "module", "m", "", "Name of module, eg: worker")
}

func createRepository(name string) {
	fileName := helpers.ToSnakeCase(name)
	file := fmt.Sprintf("repositories/%s.go", fileName)
	consumer := &Repository{
		Name: name,
	}
	if len(repositoryModuleFlag) > 0 {
		file = fmt.Sprintf("src/%s/%s", repositoryModuleFlag, file)
	}
	helpers.GenerateFile(file, tpl.Repository(), consumer)
	fmt.Println("Repository", file, "was created.")
}
