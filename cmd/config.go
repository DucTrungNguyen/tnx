package cmd

import (
	"errors"
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/DucTrungNguyen/tnx/helpers"
	"gitlab.com/DucTrungNguyen/tnx/tpl"
)

// configCmd represents the config command
var configCmd = &cobra.Command{
	Use:   "config name",
	Short: "Append config to yaml configuration file",
	Long:  `Append config to yaml configuration file`,
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			return errors.New("config name is required")
		}
		appendConfig(args[0])
		return nil
	},
}

var (
	configModuleFlag string
	configEnvFlag    string
)

type DatasourceConfig struct {
	Driver          string
	Host            string
	Port            string
	Database        string
	Username        string
	Password        string
	Params          string
	MigrationSource string
}

type RedisConfig struct {
	Host      string
	Port      string
	Database  string
	Username  string
	Password  string
	EnableTLS bool
}

type KafkaConfig struct {
	BootstrapServers string
}

func init() {
	rootCmd.AddCommand(configCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// configCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	configCmd.Flags().StringVarP(&configModuleFlag, "module", "m", "", "Name of module, Eg: internal")
	configCmd.Flags().StringVarP(&configEnvFlag, "env", "e", "", "Name of environment, Eg: local")
}

func appendConfig(name string) {
	fileName := getFileName()
	helpers.Append(fileName, getTemplate(name), getData(name))
	fmt.Println("Append", name, "config to", fileName, "successfully.")
}

func getFileName() string {
	return fmt.Sprintf("src/%s/config/%s.yml", configModuleFlag, configEnvFlag)
}

func getTemplate(name string) []byte {
	switch name {
	case "datasource":
		return tpl.DatasourceConfig()
	case "redis":
		return tpl.RedisConfig()
	case "producer":
		return tpl.ProducerConfig()
	case "consumer":
		return tpl.ConsumerConfig()
	default:
		cobra.CheckErr(fmt.Sprintf("%s config name is not support yet.", name))
		return nil
	}
}

func getData(name string) interface{} {
	switch name {
	case "datasource":
		return getDatasourceData()
	case "redis":
		return getRedisData()
	case "producer":
		return getKafkaData()
	case "consumer":
		return getKafkaData()
	default:
		cobra.CheckErr(fmt.Sprintf("%s config name is not support yet.", name))
		return nil
	}
}

func getDatasourceData() *DatasourceConfig {
	data := &DatasourceConfig{
		Driver:          "mysql",
		Host:            "localhost",
		Port:            "3306",
		Database:        "database",
		Username:        "root",
		Password:        "secret",
		Params:          "parseTime=true",
		MigrationSource: "",
	}
	switch configModuleFlag {
	case "migration":
		data.Params = "parseTime=true&multiStatements=true"
		data.MigrationSource = "file://migrations"
		break
	}
	switch configEnvFlag {
	case "qc":
		data.Host = "mysql-main.int.nonprodposi.com"
		data.Port = "3306"
		data.Password = ""
		break
	case "uat":
		data.Host = "mysql-main.int.nonprodposi.com"
		data.Port = "3306"
		data.Password = ""
		break
	case "prod":
		data.Host = "mysql-main.int.position.exchange"
		data.Port = "3306"
		data.Password = ""
		break
	case "testing":
		data.Params = "parseTime=true&multiStatements=true"
		data.MigrationSource = "file://../../migration/migrations"
		break
	}
	return data
}

func getRedisData() interface{} {
	data := &RedisConfig{
		Host:      "localhost",
		Port:      "6379",
		Database:  "0",
		Username:  "",
		Password:  "",
		EnableTLS: false,
	}
	switch configEnvFlag {
	case "qc":
		data.Host = "redis-main.int.nonprodposi.com"
		data.Port = "6379"
		data.Password = ""
		data.EnableTLS = false
		break
	case "uat":
		data.Host = "redis-main.int.nonprodposi.com"
		data.Port = "6379"
		data.Database = "1"
		data.Password = ""
		data.EnableTLS = false
		break
	case "prod":
		data.Host = "redis-main.int.position.exchange"
		data.Port = "6379"
		data.Password = ""
		data.EnableTLS = false
		break
	}
	return data
}

func getKafkaData() interface{} {
	data := &KafkaConfig{
		BootstrapServers: "localhost:29092",
	}
	switch configEnvFlag {
	case "qc":
		data.BootstrapServers = "kafka-main-01.int.nonprodposi.com:9092,kafka-main-02.int.nonprodposi.com:9092,kafka-main-03.int.nonprodposi.com:9092"
		break
	case "uat":
		data.BootstrapServers = "kafka-main-01.int.nonprodposi.com:9092,kafka-main-02.int.nonprodposi.com:9092,kafka-main-03.int.nonprodposi.com:9092"
		break
	case "prod":
		data.BootstrapServers = "kafka-main-01.int.position.exchange:9092,kafka-main-02.int.position.exchange:9092,kafka-main-03.int.position.exchange:9092"
		break
	}
	return data
}
