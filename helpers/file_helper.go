package helpers

import (
	"bytes"
	"fmt"
	"github.com/spf13/cobra"
	"os"
	"path"
	"sync"
	"text/template"
)

var (
	once                sync.Once
	projectAbsolutePath string
)

func GetProjectAbsolutePath() string {
	once.Do(func() {
		wd, err := os.Getwd()
		if err != nil {
			cobra.CheckErr(err)
		}
		projectAbsolutePath = wd
	})
	return projectAbsolutePath
}

func GenerateFile(fileName string, content []byte, data interface{}) {
	fullFileName := fmt.Sprintf("%s/%s", GetProjectAbsolutePath(), fileName)
	filePath := path.Dir(fullFileName)
	if _, err := os.Stat(filePath); os.IsNotExist(err) {
		if err := os.MkdirAll(filePath, 0754); err != nil {
			cobra.CheckErr(err)
		}
	}
	file, err := os.Create(fileName)
	if err != nil {
		cobra.CheckErr(err)
	}
	defer file.Close()
	if err := template.Must(template.New("file").Parse(string(content))).Execute(file, data); err != nil {
		cobra.CheckErr(err)
	}
}

func Append(fileName string, templateContent []byte, data interface{}) {
	file, err := os.OpenFile(fileName, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0644)
	if err != nil {
		cobra.CheckErr(err)
	}
	defer file.Close()
	buf := new(bytes.Buffer)
	if err := template.Must(template.New("paragraph").Parse(string(templateContent))).Execute(buf, data); err != nil {
		cobra.CheckErr(err)
	}
	if _, err = file.WriteString(buf.String()); err != nil {
		cobra.CheckErr(err)
	}
}
