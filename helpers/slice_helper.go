package helpers

func Contains(slc []string, str string) bool {
	for _, val := range slc {
		if val == str {
			return true
		}
	}
	return false
}
