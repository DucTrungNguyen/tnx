# DucTrungNguyen Backend CLI

## Installation

```shell
export GOPATH=$HOME/go
export PATH=$PATH:$GOPATH/bin
```

```shell
go install gitlab.com/DucTrungNguyen/tnx@latest
```

## Usage

### Init project

```shell
mkdir sample
cd sample
tnx init gitlab.com/backend/service
```

With modules

```shell
tnx init gitlab.com/backend/service -m internal,migration,worker
```

After init, run the command below to download dependencies

```shell
make tidy
```

### Create component

#### Create configuration properties

```shell
tnx properties ExampleProperties --module worker --prefix app.example
```

#### Create migration file

tnx will autodetect current version and increase version for new file

```shell
tnx migration create_users_table
```

**Conventions for migration name**

*For creating table:* create_<table_name>_table

#### Create controller

```shell
tnx controller ExampleController --module worker
```

#### Create kafka consumer

```shell
tnx consumer ExampleConsumer --module worker
```

#### Create event

```shell
tnx event ExampleEvent --module worker
```

Without modules, tnx will generate event to core module

#### Create REST resource

```shell
tnx resource ExampleResource --module public
```

#### Add config to yaml config file

```shell
tnx config datasource --module public --env local
```

Available supported config type:
- datasource
- redis
- producer
- consumer

#### Create Gorm repository

```shell
tnx repository ExampleRepository --module public
```

#### Create database model

```shell
tnx model ExampleModel --module public
```

Default module: adapter

#### Create cron job

```shell
tnx cron ExampleJob --module worker
```

Default module: worker