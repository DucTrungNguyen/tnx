package tpl

func GitlabCI() []byte {
	return []byte(`default:
  image: asia-docker.pkg.dev/DucTrungNguyen-nonprod/toolkit/gcp-deployer:v2
  tags:
    - tnx-runner

variables:
  IMAGE_TAG: $CI_COMMIT_SHORT_SHA
  GIT_SUBMODULE_STRATEGY: recursive
  SERVICE_BASE_NAME: {{ .ServiceName }}
  DOCKER_FILE: Dockerfile
  BASE_NONPROD_DOCKER_IMAGE: asia-docker.pkg.dev/DucTrungNguyen-nonprod/services/{{ .ServiceName }}
  BASE_PROD_DOCKER_IMAGE: europe-docker.pkg.dev/DucTrungNguyen-prod/services/{{ .ServiceName }}

stages:
  - verify
  - docker build
  - deploy to uat
  - tag
  - deploy to production

.test:
  stage: verify
  allow_failure: false
  image: golang:1.19-alpine
  only:
    changes:
      - src/**/*
  except:
    refs:
      - tags
  before_script:
    - apk update && apk add --no-cache git build-base
    - export GOPRIVATE=gitlab.com
    - git config --global url."https://$GITLAB_USER:$GITLAB_ACCESS_TOKEN@gitlab.com/".insteadOf "https://gitlab.com/"

automation test:
  extends: .test
  services:
    - name: mysql:8.0
      alias: mysql
    - name: krisgeus/docker-kafka@sha256:15d4d3451ef3c106db75c65cfa3f4a667ee9a246ad7365e49cf6ff547165cfc5
      alias: kafka
  variables:
    MYSQL_DATABASE: database
    MYSQL_ROOT_PASSWORD: secret
    APP_DATASOURCE_HOST: mysql
    APP_KAFKA_BOOTSTRAPSERVERS: kafka:9092
  script:{{ if .HasAdapterModule }}
    - cd $CI_PROJECT_DIR/src/adapter && go test ./...{{ end }}{{ if .HasCoreModule }}
    - cd $CI_PROJECT_DIR/src/core && go test ./...{{ end }}{{ if .HasInternalModule }}
    - cd $CI_PROJECT_DIR/src/internal && go test ./...{{ end }}{{ if .HasPublicModule }}
    - cd $CI_PROJECT_DIR/src/public && go test ./...{{ end }}{{ if .HasWorkerModule }}
    - cd $CI_PROJECT_DIR/src/worker && go test ./...{{ end }}

lint:
  extends: .test
  script:
    - go install golang.org/x/lint/golint@latest
    - golint -set_exit_status ./...

security:
  extends: .test
  script:
    - go install github.com/securego/gosec/v2/cmd/gosec@latest{{ if .HasAdapterModule }}
    - cd $CI_PROJECT_DIR/src/adapter && gosec ./...{{ end }}{{ if .HasCoreModule }}
    - cd $CI_PROJECT_DIR/src/core && gosec ./...{{ end }}{{ if .HasInternalModule }}
    - cd $CI_PROJECT_DIR/src/internal && gosec ./...{{ end }}{{ if .HasMigrationModule }}
    - cd $CI_PROJECT_DIR/src/migration && gosec ./...{{ end }}{{ if .HasPublicModule }}
    - cd $CI_PROJECT_DIR/src/public && gosec ./...{{ end }}{{ if .HasWorkerModule }}
    - cd $CI_PROJECT_DIR/src/worker && gosec ./...{{ end }}

.docker_build:
  stage: docker build
  only:
    refs:
      - develop
      - main
  script:
    - export BUILD_TIME=$(date +"%s")
    - export BUILD_COMMIT_HASH=$CI_COMMIT_SHORT_SHA
    - envsubst < k8s/docker-build.yml > /tmp/docker-build.yml
    - gcloud beta builds submit --config=/tmp/docker-build.yml

{{ if .HasInternalModule }}build internal:
  extends: .docker_build
  variables:
    MODULE: internal
    IMAGE_NAME: $BASE_NONPROD_DOCKER_IMAGE-internal

{{ end }}{{ if .HasMigrationModule }}build migration:
  extends: .docker_build
  variables:
    DOCKER_FILE: Dockerfile.migration
    IMAGE_NAME: $BASE_NONPROD_DOCKER_IMAGE-migration

{{ end }}{{ if .HasPublicModule }}build public:
  extends: .docker_build
  variables:
    MODULE: public
    IMAGE_NAME: $BASE_NONPROD_DOCKER_IMAGE-public

{{ end }}{{ if .HasWorkerModule }}build worker:
  extends: .docker_build
  variables:
    MODULE: worker
    IMAGE_NAME: $BASE_NONPROD_DOCKER_IMAGE-worker

{{ end }}.deploy_to_uat:
  stage: deploy to uat
  only:
    - main
  variables:
    HOST: apex-uat.position.exchange
    APP_PROFILES: uat
    NAMESPACE: uat

{{ if .HasMigrationModule }}deploy migration to uat:
  extends: .deploy_to_uat
  variables:
    SERVICE_NAME: $SERVICE_BASE_NAME-migration
    IMAGE_NAME: $BASE_NONPROD_DOCKER_IMAGE-migration
    APP_PROFILES: uat
  script: |
    cd k8s/migration && bash migrate.sh

{{ end }}{{ if .HasInternalModule }}deploy internal to uat:
  extends: .deploy_to_uat
  variables:
    SERVICE_NAME: $SERVICE_BASE_NAME-internal
    SERVICE_PATH: /{{ .ServiceName }}-internal
    IMAGE_NAME: $BASE_NONPROD_DOCKER_IMAGE-internal
  script: |
    cd k8s/internal && bash deploy.sh

{{ end }}{{ if .HasPublicModule }}deploy public to uat:
  extends: .deploy_to_uat
  variables:
    SERVICE_NAME: $SERVICE_BASE_NAME-public
    SERVICE_PATH: /{{ .ServiceName }}
    IMAGE_NAME: $BASE_NONPROD_DOCKER_IMAGE-public
  script: |
    cd k8s/public && bash deploy.sh

{{ end }}{{ if .HasWorkerModule }}deploy worker to uat:
  extends: .deploy_to_uat
  variables:
    SERVICE_NAME: $SERVICE_BASE_NAME-worker
    SERVICE_PATH: /{{ .ServiceName }}-worker
    IMAGE_NAME: $BASE_NONPROD_DOCKER_IMAGE-worker
  script: |
    cd k8s/worker && bash deploy.sh

{{ end }}.tag:
  image: europe-docker.pkg.dev/DucTrungNguyen-prod/toolkit/gcp-deployer:latest
  stage: tag
  only:
    - tags
  script:
    - gcloud container images add-tag $SOURCE_IMAGE:$IMAGE_TAG $TARGET_IMAGE:$CI_COMMIT_TAG --quiet

{{ if .HasInternalModule }}tag internal:
  extends: .tag
  variables:
    SOURCE_IMAGE: $BASE_NONPROD_DOCKER_IMAGE-internal
    TARGET_IMAGE: $BASE_PROD_DOCKER_IMAGE-internal

{{ end }}{{ if .HasMigrationModule }}tag migration:
  extends: .tag
  variables:
    SOURCE_IMAGE: $BASE_NONPROD_DOCKER_IMAGE-migration
    TARGET_IMAGE: $BASE_PROD_DOCKER_IMAGE-migration

{{ end }}{{ if .HasPublicModule }}tag public:
  extends: .tag
  variables:
    SOURCE_IMAGE: $BASE_NONPROD_DOCKER_IMAGE-public
    TARGET_IMAGE: $BASE_PROD_DOCKER_IMAGE-public

{{ end }}{{ if .HasWorkerModule }}tag worker:
  extends: .tag
  variables:
    SOURCE_IMAGE: $BASE_NONPROD_DOCKER_IMAGE-worker
    TARGET_IMAGE: $BASE_PROD_DOCKER_IMAGE-worker

{{ end }}request to deploy to production:
  stage: deploy to production
  when: manual
  only:
    - tags
  variables:
    PROJECT: {{ .ServiceName }}
  script:
    - bash /request-production.sh $CI_COMMIT_TAG
`)
}
