package tpl

func K8SWebserviceConfigmap() []byte {
	return []byte(`apiVersion: v1
kind: ConfigMap
metadata:
  name: $SERVICE_NAME
data:
  APP_PROFILES: $APP_PROFILES
  APP_PATH: $SERVICE_PATH
`)
}
