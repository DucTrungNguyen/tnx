package tpl

func RedisConfig() []byte {
	return []byte(`
  redis:
    host: {{ .Host }}
    port: {{ .Port }}
    database: {{ .Database }}{{ if ne .Username "" }}
    username: {{ .Username }}{{ end }}{{ if ne .Password "" }}
    password: {{ .Password }}{{ end }}{{ if .EnableTLS }}
    enableTLS: true{{end}}
`)
}
