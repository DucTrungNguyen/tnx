package tpl

func ProducerConfig() []byte {
	return []byte(`
  kafka:
    bootstrapServers: {{ .BootstrapServers }}
    clientId: client-id
    admin:
      topics:
        - name: topic
          partitions: 3
          replicaFactor: 1
          retention: 24h
    producer:
      eventMappings:
        ExampleEvent:
          topicName: topic
          transactional: false
`)
}
