package tpl

func DatasourceConfig() []byte {
	return []byte(`
  datasource:
    driver: {{ .Driver }}
    host: {{ .Host }}
    port: {{ .Port }}
    database: {{ .Database }}
    username: {{ .Username }}{{ if ne .Password "" }}
    password: {{ .Password }}{{ end }}
    params: {{ .Params }}{{ if ne .MigrationSource "" }}
    migrationSource: {{ .MigrationSource }}{{ end }}
`)
}
