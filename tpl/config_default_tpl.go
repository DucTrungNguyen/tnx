package tpl

func ConfigDefault() []byte {
	return []byte(`app:
  name: Application Name
  security:
    http:
      publicUrls:
        - /actuator/health
        - /actuator/info
        - /swagger/*
`)
}
