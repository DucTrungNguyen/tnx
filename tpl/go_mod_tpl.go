package tpl

func GoMod() []byte {
	return []byte(`module {{ .Module }}

go 1.19
`)
}
