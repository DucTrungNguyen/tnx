package tpl

func Migration() []byte {
	return []byte("{{ if eq .Action `CreateTable` }}CREATE TABLE IF NOT EXISTS `{{ .Table }}`\n" +
		"(\n" +
		"    `id`             BIGINT UNSIGNED   NOT NULL AUTO_INCREMENT PRIMARY KEY,\n" +
		"    `created_at`     DATETIME          NOT NULL DEFAULT CURRENT_TIMESTAMP,\n" +
		"    `updated_at`     DATETIME          NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP\n" +
		");{{ end }}")
}
