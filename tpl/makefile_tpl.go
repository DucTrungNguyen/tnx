package tpl

func Makefile() []byte {
	return []byte(`up:
	docker-compose up

down:
	docker-compose down

tidy:{{ if .HasCoreModule }}
	cd src/core && go mod tidy{{ end }}{{ if .HasAdapterModule }}
	cd src/adapter && go mod tidy{{ end }}{{ if .HasMigrationModule }}
	cd src/migration && go mod tidy{{ end }}{{ if .HasInternalModule }}
	cd src/internal && go mod tidy{{ end }}{{ if .HasPublicModule }}
	cd src/public && go mod tidy{{ end }}{{ if .HasWorkerModule }}
	cd src/worker && go mod tidy{{ end }}

test:{{ if .HasCoreModule }}
	cd src/core && go mod tidy && go test ./...{{ end }}{{ if .HasAdapterModule }}
	cd src/adapter && go mod tidy && go test ./...{{ end }}{{ if .HasMigrationModule }}
	cd src/migration && go mod tidy && go test ./...{{ end }}{{ if .HasInternalModule }}
	cd src/internal && go mod tidy && go test ./...{{ end }}{{ if .HasPublicModule }}
	cd src/public && go mod tidy && go test ./...{{ end }}{{ if .HasWorkerModule }}
	cd src/worker && go mod tidy && go test ./...{{ end }}

swagger-internal:
	cd src/internal && swag init --parseDependency --parseDepth=3

swagger-public:
	cd src/public && swag init --parseDependency --parseDepth=3
`)
}
