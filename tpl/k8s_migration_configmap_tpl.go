package tpl

func K8SMigrationConfigmap() []byte {
	return []byte(`apiVersion: v1
kind: ConfigMap
metadata:
  name: $SERVICE_NAME
data:
  APP_PROFILES: $APP_PROFILES
`)
}
