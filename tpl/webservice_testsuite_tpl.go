package tpl

func WebserviceTestsuite() []byte {
	return []byte(`package tests

import (
	"context"
	"gitlab.com/golibs-starter/golib"
	"gitlab.com/golibs-starter/golib-test"
	"{{ .Module }}/bootstrap"
	"go.uber.org/fx"
)

func init() {
	err := fx.New(
		bootstrap.All(),
		golib.ProvidePropsOption(golib.WithActiveProfiles([]string{"testing"})),
		golib.ProvidePropsOption(golib.WithPaths([]string{"../config/"})),
		golibtest.EnableWebTestUtil(),
	).Start(context.Background())
	if err != nil {
		panic(err)
	}
}
`)
}
