package tpl

func ConsumerConfig() []byte {
	return []byte(`
  kafka:
    bootstrapServers: {{ .BootstrapServers }}
    clientId: client-id
    consumer:
      handlerMappings:
        ConsumerName:
          topic: topic
          groupId: groupId
          enable: true
`)
}
