package tpl

func Resource() []byte {
	return []byte(`// Source code template generated by https://gitlab.com/DucTrungNguyen/tnx

package resources

// {{ .Name }} represents
type {{ .Name }} struct {
}

// New{{ .Name }}Item return a single {{ .Name }} resource
func New{{ .Name }}Item() *{{ .Name }} {
	return nil
}

// New{{ .Name }}Collection return a collection of {{ .Name }} resource
func New{{ .Name }}Collection() []*{{ .Name }} {
	return nil
}
`)
}
