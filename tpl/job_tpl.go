package tpl

func Job() []byte {
	return []byte(`package jobs

import (
	"gitlab.com/golibs-starter/golib/log"
	"time"
)

// {{ .Name }} ...
type {{ .Name }} struct {
}

// New{{ .Name }} ...
func New{{ .Name }}() *{{ .Name }} {
	return &{{ .Name }}{}
}

// Run job handler
func (j *{{ .Name }}) Run() {
	start := time.Now()
	log.Infof("[{{ .Name }}] job start")
	log.Infow([]interface{}{"execution_time", time.Now().Sub(start).Milliseconds()}, "[{{ .Name }}] job stop")
}
`)
}
